<?php
/*
Plugin Name: MLS API Test plugin
Plugin URI: 
Description: MLS API Test plugin.
Author: Agile Solutions PK
Version: 1.1
Author URI: http://agilesolutionspk.com
*/

if ( !class_exists( 'Mls_Api_test_plugins' )){
	class Mls_Api_test_plugins{
		
		function __construct() {
		
			add_action( 'admin_init', array(&$this, 'admin_init') );
			add_action('init', array(&$this, 'fe_init'));
			add_action( 'admin_menu', array(&$this, 'admin_menu') );
		}
		
		function compute_signature($url, $auth_token, $data = array()){
			// sort the array by keys
			ksort($data);

			// append them to the data string in order
			// with no delimiters
			foreach($data as $key => $value)
				$url .= "$key$value";
				
			// This function calculates the HMAC hash of the data with the key
			// passed in
			// Note: hash_hmac requires PHP 5 >= 5.1.2 or PECL hash:1.1-1.5
			// Or http://pear.php.net/package/Crypt_HMAC/
			return base64_encode(hash_hmac("sha1", $url, $auth_token, true));
		}
		
		function build_post_vars(){
			$pv = array();
			
			foreach($_POST as $k => $v){
				$pv[$k] = $v;
			}
			unset($pv['mls_submit']);
			unset($pv['auth_sign']);
			
			return $pv;
		}
		
		
		function mls_api_test_post_form(){
			$url = "http://agilesolutionspk.com/qa15/api/";
			$btn_txt = "Compute Signature";
			$tgt = "";
			
			if(isset($_POST['mls_submit'])){
				$tgt = $url;
				$btn_txt = "Send Test";
				$data = $this->build_post_vars();
				
				$sign = $this->compute_signature($url,'e6ad8dde4a4522d13687a6c5cefb2eea', $data);
			}
		?>
			<form method = "post" action = "<?php echo $tgt;?>" />
				<div style="margin-bottom:2em;">
					<?php if(empty($tgt)){?>
						Action : <select style="width:20em;" name="action">
							<option value = "insert">Insert</option>
							<option value = "update" >Update</option>
							<option value = "find" >Find</option>
						</select>
					<?php }else{ ?>
						Action : <input style="width:20em;" type="text" name="action" value="<?php echo $_POST['action'];?>" readonly />
					
					<?php } ?>
				</div>
				
				<div style="margin-bottom:2em;">
					mls_id : <input style="width:20em;" type="text" name="mls_id" value="<?php echo $_POST['mls_id'];?>"  />
				</div>
				
				<div style="margin-bottom:2em;">
					zip_code : <input style="width:20em;" type="text" name="zip_code" value="<?php echo $_POST['zip_code'];?>"  />
				</div>
				<div style="margin-bottom:2em;">
					year_built : <input style="width:20em;" type="text" name="year_built" value="<?php echo $_POST['year_built'];?>"  />
				</div>
				<div style="margin-bottom:2em;">
					street_name : <input style="width:20em;" type="text" name="street_name" value="<?php echo $_POST['street_name'];?>"  />
				</div>
				
				
				<div style="margin-bottom:2em;">
					E-mail : <input style="width:20em;" type="text" name="auth_email" value="<?php echo $_POST['auth_email'];?>" required />
				</div>
				<div style="margin-bottom:2em;">
					auth-sign : <input style="width:20em;" type="text" name="auth_sign" readonly  value="<?php echo $sign;?>"/>
				</div>
				<div>
					Submit : <input type="submit" name="mls_submit" value="<?php echo $btn_txt;?>" />
				</div>
			</form>
		<?php
		}
		
		function fe_init(){
			
		}
		
		function admin_init(){
			
		}
		
		function admin_menu() {
			add_menu_page('MLS API TEST', 'MLS API TEST', 'manage_options', 'aspk_mls_api_test',array(&$this, 'mls_api_test_post_form'));
		}
		
		
		
	} //class ends
} //class exists ends
new Mls_Api_test_plugins();



?>
