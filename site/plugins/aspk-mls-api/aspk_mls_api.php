<?php
/*
Plugin Name: MLS API Server
Plugin URI: 
Description: This is MLS API Server.
Author: Agile Solutions PK
Version: 1.6
Author URI: http://agilesolutionspk.com
*/

if ( !class_exists( 'Mls_Api_Server' )){
	class Mls_Api_Server{
		
		function __construct() {

			add_action( 'admin_init', array(&$this, 'admin_init') );
			add_action('init', array(&$this, 'fe_init'));
			add_action( 'admin_menu', array(&$this, 'admin_menu') );
			add_action('admin_enqueue_scripts', array(&$this, 'admin_enqueue_scripts') );
			add_action('wp_enqueue_scripts', array(&$this, 'frontend_scripts') );
			register_activation_hook( __FILE__, array($this, 'install') );
			//register_deactivation_hook( __FILE__, array($this, 'de_activate') );
			add_shortcode( 'aspk_mls_api_reception', array(&$this, 'aspk_mls_api_reception') );
			add_filter('manage_users_columns', array(&$this, 'add_to_user_colomn'));
			add_filter('manage_users_custom_column', array(&$this, 'add_to_user_colomn_value') , 99 , 3);
			add_action( 'admin_notices', array(&$this, 'admin_notices'));
			if($this->enable_aspk_db()){
				update_option('_aspk_mls_api_status','running');
			}else{
				update_option('_aspk_mls_api_status','db not enabled');
			}
		}
		
		function fe_init(){
			ob_start();
		}
		
		function admin_init(){
			
		}
		
		function admin_menu() {
			add_menu_page('MLS API', 'MLS API', 'manage_options', 'aspk_mls_api_status',array(&$this, 'show_server_status'),plugins_url( 'img/api_img.jpg',__FILE__ ));
			add_submenu_page('', 'Token Generate','Token Generate', 'manage_options', 'aspk_mls_api_token', array(&$this, 'token_show_to_user'));
			add_submenu_page('aspk_mls_api_status','Database Settings', 'Database Settings', 'manage_options', 'aspk_mls_db_settings', array(&$this,'database_setting'));
		}
		
		function admin_notices(){
			
			if(! $this->is_set_db_settings()){
				?>
					<div class="error">
						<p>MLS Database Settings are not set.</p>
					</div>
				<?php
				
			}
			$opt = get_option( '_aspk_mls_api_status', 'none' );
			if($opt != 'running'){
				?>
					<div class="error">
						<p>MLS API Server NOT Running</p>
					</div>
				<?php
				
			}
		}
		
		function show_server_status(){
			$st = get_option( '_aspk_mls_api_status', 'Not Running' );
			?>
				<div><h1>MLS API Server Status</h1></div><br/><br/>
				<div class="update">
					<p>Server is <?php echo ucwords($st);?></p>
				</div>
			<?php
			
		}
		
		function is_set_db_settings(){
			$s = get_option( '_aspk_mls_db_settings', array('password' => '' ));
			if(empty($s['password'])) return false;
			return true;
		}
		
		
		
		function database_setting(){
			$s = $this->save_database_settings();
			//s is key value array
			?>
				<h1>MLS Database Settings</h1>
				<div id="msg"><h3><?php echo $s['msg'];?></h3></div>
				<div style = "clear:left;float:left;">
					<form method="POST">
						<div style = "clear:left;margin-top:1em;"><span style="display:inline-block;width:10em;">Db Name:</span><input style="width: 20em;" type="text" value="<?php echo $s['db_name'];?>" name="db_name" placeholder="Keep blank for default database" ></div>
						<div style = "clear:left;margin-top:1em;"><span style="display:inline-block;width:10em;">Host Name:</span><input style="width: 20em;" type="text" value="<?php echo $s['host_name'];?>"" name="host_name"  required ></div>
						<div style = "clear:left;margin-top:1em;"><span style="display:inline-block;width:10em;">User Id:</span><input style="width: 20em;" type="text" value="<?php echo $s['user_id'];?>"" name="user_id"  required ></div>
						<div style = "clear:left;margin-top:1em;"><span style="display:inline-block;width:10em;">Password:</span><input style="width: 20em;" type="text" value="<?php echo $s['password'];?>"" name="password"  required ></div>
						<div style = "clear:left;margin-top:1em;margin-left:10em;"><input type="submit" name="save_db_settings" value="Save Settings" class="button button-primary"></div>
					</form>
				</div>
			<?php
		}
		
		function save_database_settings(){
			$default = array(
							'db_name' => '',
							'host_name' => '',
							'user_id' => '',
							'password' => '',
							'msg' => '',
						);
			if(isset($_POST['save_db_settings'])){
				$posted_settings = array(
							'db_name' => $_POST['db_name'],
							'host_name' => $_POST['host_name'],
							'user_id' => $_POST['user_id'],
							'password' => $_POST['password'],
						);
				update_option( '_aspk_mls_db_settings', $posted_settings );
				$posted_settings['msg'] = 'Settings have been saved';
				return $posted_settings;
			}
			$s = get_option( '_aspk_mls_db_settings', $default );
			$s['msg'] = '';
			return $s;
		}
		
		
		
		function token_show_to_user() {
			if(! isset(	$_GET['uid'])){
				echo "<div>Malformed Request</div>";
				return;
			}
			
			$user_id = 	$_GET['uid'];
			
			if(intval($user_id) < 1){
				echo "<div>Malformed Request</div>";
				return;
			}
			
			$token = $this->generate_token($user_id);
			echo "<h1>Generate Token</h1>";
			echo '<p><br/><br/>New Token: <h2>'.$token. '</h2></p></br><p>Token Generated Successfully. It will replace any previously generated Token</p>';
		}
		 
		function add_to_user_colomn($columns){
			$new_columns = array();
			foreach($columns as $k=>$v){
				 $new_columns[$k] = $v;
				if($k == 'name'){
					$new_columns['token_generate'] = 'Generate Token';
				}
			}
			return $new_columns;
		
		}
		
		function add_to_user_colomn_value($empty, $column_name, $user_id){
			
			$url = admin_url('/admin.php?page=aspk_mls_api_token&uid='.$user_id);
			if ( $column_name == 'token_generate' ) {    
				return $colomn_value = '<a href="'.$url.'">Get Token</a>';
			}
		}
		
		function admin_enqueue_scripts(){
			wp_enqueue_script('jquery');
			wp_enqueue_script('agile_mls_admin', plugins_url('js/admin.js', __FILE__) );
		}
		
		
		
		function frontend_scripts(){
			wp_enqueue_script('jquery');

		}
		
		function aspk_mls_api_reception(){
			
			ob_end_clean();
			if(! $this->validate_post()) exit;
			
			$this->authenticate();
			$this->process_api_request();
			exit;
		}
		
		function validate_post(){
			
			if(! isset($_POST['action']) || ! isset($_POST['auth_email']) || ! isset($_POST['auth_sign'])){
				$this->send_response("400", "Request not properly formed. Please API Documentation");
				return false;
			}
			
			$action = $_POST['action'];
			
			switch(strtolower($action)){
				case "insert":
				case "update":
				case "find":
					break;
				default:
					$this->send_response("405", "Method not supported");
					return false; //incorrect action value
			}
			return true;
		}
		
		function authenticate(){
			global $post;
			
			$url= get_permalink($post);
			$auth_email = $_POST['auth_email'];
			$auth_sign = $_POST['auth_sign'];
			
			$user = get_user_by( 'email', $auth_email );
			if(! $user){
				$this->send_response("401", "Validation Failed, User or Token Invalid");
				return false;
			}
			
			$auth_token = get_user_meta($user->ID, 'aspk_mls_api_token', true);
			if(empty($auth_token)){
				$this->send_response("401", "UnAuthorized. Contact Support");
				return false;
			}
			
			$post_vars = $this->build_post_vars();
			
			$validator = new Aspk_Request_Validator($auth_token);
			
			//echo "$url <pre>";print_r($post_vars);
			//echo $validator->compute_signature($url, $post_vars);
			if(! $validator->validate($auth_sign, $url, $post_vars)){
				$this->send_response("401", "Validation Failed");
				
			}
			
		}
		
		function build_post_vars(){
			//$vars = array();
			
			//if(isset($_POST['action'])) $vars['action'] = $_POST['action'];
			//if(isset($_POST['auth_email'])) $vars['auth_email'] = $_POST['auth_email'];
			//if(isset($_POST['auth_sign'])) $vars['auth_sign'] = $_POST['auth_sign'];
			
			if(isset($_POST['auth_sign'])) unset($_POST['auth_sign']); //auth is not part of hash
			if(isset($_POST['mls_submit'])) unset($_POST['mls_submit']); //remove test
			
			$kv_row = array_change_key_case($_POST, CASE_LOWER);
			
			return $kv_row;
			
		}
		
		
		function process_api_request(){
			$action = $_POST['action'];
			
			switch(strtolower($action)){
				case "insert":
					$this->insert_data_from_request();
					break;
				case "update":
					$this->update_data_from_request();
					break;
				case "find":
					$this->search_data_from_request();
					break;
				default:
					$this->send_response("405", "Method not supported");
					return false; //incorrect action value
			}
			
		}
		
		function search_data_from_request(){
			global $mls_importer;
			global $aspk_db;
			
			$kv_row = $this->build_post_vars();
			
			$perfect_kv_row = $mls_importer->remove_unwated_cols_from_kv_row($kv_row);
			
			if(count($perfect_kv_row) < 1){
				$this->send_response('420','No valid field found to search for');
				return;
			}
			
			$where = $this->prepare_where($perfect_kv_row);
			
			$sql = "select * from mls_data where ". $where;
			
			
			$results = $aspk_db->get_results($sql);
			
			if(empty($results)){
				$this->send_response('200','No resuls found');
				return;
			}
			
			$this->send_response('200','OK', $results);
			return;
		}
		
		function prepare_where($kv_row){
			$parts = array();
			
			foreach($kv_row as $k=>$v){
				if(empty($v)) continue;  //skip null
				
				$parts[] = "$k like '%".$v."%'";
			}

			$where = implode(' OR ',$parts);
			
			return $where;
		}
		
		function update_data_from_request(){
			global $mls_importer;
			global $aspk_db;
			
			$kv_row = $this->build_post_vars();
			
			if (! array_key_exists('mls_id', $kv_row)) {
				$this->send_response('420','Cannot Update, mls_id not found');
				return;
			}
			
			if(! $mls_importer->is_available($kv_row['mls_id'])){
				$this->send_response('420','Cannot Update, mls_id does not exist');
				return;
			}
			
			$perfect_kv_row = $mls_importer->remove_unwated_cols_from_kv_row($kv_row);
			
			$updated = $aspk_db->update( 'mls_data', $perfect_kv_row, array('mls_id' => $kv_row['mls_id']));
			
			if($updated === false){
				$this->send_response('420','Failed to Update. DB Error');
			}
			
			$this->send_response('200','OK');
			return;
		}
		
		
		
		function insert_data_from_request(){
			global $mls_importer;
			global $aspk_db;
			
			$kv_row = $this->build_post_vars();
			
			if (! array_key_exists('mls_id', $kv_row)) {
				$this->send_response('420','Failed to Insert. mls_id not found');
				return;
			}
			
			if($mls_importer->is_available($kv_row['mls_id'])){
				$this->send_response('420','Failed to Insert. mls_id already exists');
				return;
			}
			
			$perfect_kv_row = $mls_importer->remove_unwated_cols_from_kv_row($kv_row);
			
			$inserted = $aspk_db->insert( 'mls_data', $perfect_kv_row);
			
			if($inserted === false){
				$this->send_response('420','Failed to Insert. DB Error');
				
			}
			$this->send_response('200','OK');
			return;
		}
		
		function generate_token($user_id){
			$num = rand(100000000000, 999999999999);
			$token = md5($num);
			update_user_meta( $user_id, 'aspk_mls_api_token', $token);
			return $token;
		}
		
		function enable_aspk_db(){
			global $aspk_db;
			
			$s = get_option( '_aspk_mls_db_settings', NULL );
			
			if(! $s) return false;
			
			try{
				if(! isset($aspk_db)){
					$aspk_db = new wpdb($s['user_id'],$s['password'],$s['db_name'],$s['host_name']);
				}
				//$aspk_db->show_errors();
			}catch(Exception $e){
				return false;
			}
			return true;
		}
		
		function install(){
			global $aspk_db;
			
			update_option('_aspk_mls_api_status','starting');
			
		}
		
		function send_response($status, $msg, $data = NULL ){
			/*
				200 OK
				400 Bad Request
				401 Unauthorized/Authentication Failed
				405 Method Not Allowed
				420 Method Failure
			*/
			
			$ret_arr = array();
			$ret_arr["status"] = "$status";
			$ret_arr["message"] = "$msg";
			if($data){
				$ret_arr["message"] = $data;
			}
			echo json_encode($ret_arr);
			exit;
		}
		
		
	} //class ends
} //class exists ends

if ( !class_exists( 'Aspk_Request_Validator' )){
	class Aspk_Request_Validator{

		protected $AuthToken;

		function __construct($token){
			$this->AuthToken = $token;
		}
		
		public function compute_signature($url, $data = array()){
			// sort the array by keys
			ksort($data);

			// append them to the data string in order
			// with no delimiters
			foreach($data as $key => $value)
				$url .= "$key$value";
				
			// This function calculates the HMAC hash of the data with the key
			// passed in
			// Note: hash_hmac requires PHP 5 >= 5.1.2 or PECL hash:1.1-1.5
			// Or http://pear.php.net/package/Crypt_HMAC/
			return base64_encode(hash_hmac("sha1", $url, $this->AuthToken, true));
		}

		public function validate($expectedSignature, $url, $data = array()){
			return $this->compute_signature($url, $data)
				== $expectedSignature;
		}

	}//ends request validator
}// if request validator

if(! isset($aspk_db)) $aspk_db = null;


new Mls_Api_Server();

?>
