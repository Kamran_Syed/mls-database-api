<?php
/*
Plugin Name: MLS CSV Importer
Plugin URI: 
Description: This plugin imports CSV files into MLS database.
Author: Agile Solutions PK
Version: 1.6
Author URI: http://agilesolutionspk.com
*/

if ( !class_exists( 'Mls_Csv_Importer' )){
	class Mls_Csv_Importer{
		private $tbl_fileds = NULL;  //kv holds table name as key and an array of feilds as value
		private $faulty_FMLS;
		private $faulty_rows = array();
		private $field_mapping = NULL; //holds field mapping
		
		function __construct() {
			
			add_action( 'admin_init', array(&$this, 'admin_init') );
			add_action('init', array(&$this, 'fe_init'));
			add_action( 'admin_menu', array(&$this, 'admin_menu') );
			add_action('admin_enqueue_scripts', array(&$this, 'admin_enqueue_scripts') );
			add_action('wp_enqueue_scripts', array(&$this, 'frontend_scripts') );
			add_action( 'admin_notices', array(&$this, 'admin_notices'));
			register_activation_hook( __FILE__, array($this, 'install') );
			//register_deactivation_hook( __FILE__, array($this, 'de_activate') );
			
			ini_set('max_execution_time', 3600);
			ini_set('upload_max_filesize', '10M');
			ini_set('post_max_size', '10M');
			ini_set('max_input_time', 300);
			
		}
		
		function fe_init(){
			//ob_start();
		}
		
		function admin_init(){
			
		}
		
		function admin_menu() {
			
			add_submenu_page('aspk_mls_api_status','Upload MLS CSV', 'Upload MLS CSV', 'manage_options', 'aspk_upload_mls_csv', array(&$this, 'multi_upload_file_handler') );
		}
		
		function admin_notices(){
			
			if(! is_plugin_active( 'aspk-mls-api/aspk_mls_api.php' )){
				?>
					<div class="error">
						<p>MLS CSV Importer depends upon MLS API Server. Please check</p>
					</div>
				<?php
			}
		}
		
		
		
		function multi_upload_file_handler(){
			global $aspk_db;
			
			if(! $this->is_set_db_settings()){
				?>
					<div class="error">
						<p>MLS Database Settings are not set. Please set and save settings.</p>
					</div>
				<?php
				return;
			}
			
			if(! $this->enable_aspk_db()){
				echo "<div style='color:red;'><h2>Database Error: ".$aspk_db->last_error."</h2></div>";
				return;
			}
			
			if(isset($_POST['submit'])){
				echo "<div ><h1>Upload MLS CSV</h1></div>";

				$uploadedfiles = $_FILES['data'];
				$no_of_file_upload = count($uploadedfiles['name']);
				$upld_file_onebyone = array();
				
				for($x=0; $x< $no_of_file_upload; $x++){
					$upld_file_onebyone ['name'] = $uploadedfiles['name'][$x]; 
					$upld_file_onebyone ['type'] = $uploadedfiles['type'][$x]; 
					$upld_file_onebyone ['tmp_name'] = $uploadedfiles['tmp_name'][$x]; 
					$upld_file_onebyone ['error'] = $uploadedfiles['error'][$x]; 
					$upld_file_onebyone ['size'] = $uploadedfiles['size'][$x]; 
					$get_all_file_upld[] = $upld_file_onebyone;
				}
				try{
					foreach($get_all_file_upld as $upld_file){
						$upload_overrides = array( 'test_form' => false );
						$movefile = wp_handle_upload( $upld_file, $upload_overrides );
						 if ($movefile){

							$file_path = $movefile['file'];
							$file_arr = $this->csv_to_array($file_path, "\t");
							unlink($file_path); //we dont want too many orphen csv files
							
							if(! $file_arr) continue; //file does not have records
							
							$this->process_csv_file_kv_arr($file_arr);
							$this->show_faulty_row_numbers();
						} 
					}
				}catch(Exception $e){
					echo "<pre>";print_r($e);
				}
				echo "<h2>File(s) Import Process Completed</h2>";
				$this->multi_upload_files('hide_header');
				
			}else{
				$this->multi_upload_files();
			}
		}
		
		function show_faulty_row_numbers(){
			if(count($this->faulty_rows)){
				$num_str = implode(',', $this->faulty_rows);
				echo "<div>Row Numbers with incorrect field count/Malformed data</div>";
				echo "<div>".$num_str."</div>";
				$this->faulty_rows = array();
			}
		}
		
		
		function process_csv_file_kv_arr(&$file_arr){
			//It is a key value array. key is field name
			
			foreach($file_arr as $kv_row){
			
				if($this->is_available($kv_row['mls_id'])){
					$this->update_mls_row($kv_row);
				}else{
					$this->insert_mls_row($kv_row);
				}
			
			}
			
		}
		
		function is_available($mls_id){
			//INT
			global $aspk_db;
			
			$sql = "Select mls_id from mls_data where mls_id = {$mls_id}";
			$r = $aspk_db->get_var($sql);
			if($r) return true;
			return false;
		}
		
		
		function insert_mls_row(&$kv_row){
			//This is one row with kay and values. Key is table columns
			global $aspk_db;
			
			
			$perfect_kv_row = $this->remove_unwated_cols_from_kv_row($kv_row);
			
			$inserted = $aspk_db->insert( 'mls_data', $perfect_kv_row);
			
			if($inserted === false){
				echo "<div>Failed to Insert: ".$aspk_db->print_error()."</div>"; 
				echo "<div>".implode(',', array_values($perfect_kv_row))."</div>";
			}
			
		}
		
		function update_mls_row(&$kv_row){
			//This is one row with kay and values. Key is table columns
			global $aspk_db;
			
			
			$perfect_kv_row = $this->remove_unwated_cols_from_kv_row($kv_row);
			
			$updated = $aspk_db->update( 'mls_data', $perfect_kv_row, array('mls_id' => $kv_row['mls_id']));
			
			if($updated === false){
				echo "<div>Failed to Update: ".$aspk_db->print_error()."</div>"; 
				echo "<div>".implode(',', array_values($perfect_kv_row))."</div>";
			}
		}
		
		function remove_unwated_cols_from_kv_row($kv_row){
			//This is one row with kay and values. Key is table columns
			$new_kv_row = array();
			
			$mapping = $this->field_mapping;
			if(! $mapping){
				$mapping = $this->get_date_field_mapping();
			}
			
			
			foreach($kv_row as $k => $v){
				
				if($this->is_table_field('mls_data', $k)){
					
					if(isset($mapping[strtolower($k)])){
						$field_obj = $mapping[strtolower($k)];
						
						if(trim($field_obj->d_type) == 'date'){
							
							$date = DateTime::createFromFormat('m/d/Y', trim($v));
						
							if($date !== false) $v = $date->format('Y-m-d');
							
							
						}
					}
					
					$new_kv_row[$k] = $v;
				}
			}
			
			return $new_kv_row;
		}
		
		function replace_null_by_empty_in_kv_arr($kv_row){
			//key value array only 1 element/row
			
			foreach ($kv_row as $key => $value) {
				if (is_null($value)) {
					 $kv_row[$key] = "";
				}
			}
			return $kv_row;
		}
		
		
		function multi_upload_files($repeat_form = NULL){
			if(! $repeat_form){
			?>
				<div ><h1>Upload MLS CSV</h1></div>
			<?php } ?>
			
				<form id = "myform" enctype="multipart/form-data" method="POST">
					<div><h4>Select file(s) to Upload (TAB delimited files only)</h4></div>
						<div id="text"> 
							<div style="margin-bottom: 1em;"><input required name="data[]" type="file" /></div>
							<!--This is where the new file field will appear -->
						</div>
					
					<a id="add-file-field" href="#">Click To Upload Another File</a>
					<!--<input type='hidden' name="action" value="uploadfiles" />-->
					<br/><br/>
					<input type="submit" name = "submit" value="Upload File(s)" class = "button button-primary"/>
				</form>
			<?php
		}
		
		function admin_enqueue_scripts(){
			wp_enqueue_script('jquery');
			wp_enqueue_script('agile_mls_uploader', plugins_url('js/dynamic_file_upload.js', __FILE__) );
			wp_enqueue_script('agile_mls_admin', plugins_url('js/admin.js', __FILE__) );
			
		}
		function frontend_scripts(){
			wp_enqueue_script('jquery');
		}
		
		function enable_aspk_db(){
			global $aspk_db;
			
			$s = get_option( '_aspk_mls_db_settings', NULL );
			
			if(! $s) return false;
			
			try{
				if(! isset($aspk_db)){
					$aspk_db = new wpdb($s['user_id'],$s['password'],$s['db_name'],$s['host_name']);
				}
				//$aspk_db->show_errors();
			}catch(Exception $e){
				return false;
			}
			return true;
		}
		
		function install(){
			global $aspk_db;
			
			//we will not create table on install
			
		}
		
		function csv_to_array($filename='', $delimiter=','){
			//returns key value array. key is the name of field
			
			?>
				<div>Processing File: <b><?php echo basename($filename);?></b></div>   
			<?php
			if(!file_exists($filename) || !is_readable($filename)){
				echo "<div style='color:red;'>File does not exist or not readable, skipping</div>";
				return FALSE;
			}
			$header = NULL;
			$data = NULL;
			$n = 0;
			
			if (($handle = fopen($filename, 'r')) !== FALSE){
			
				while (($row = fgetcsv($handle, 10000, $delimiter)) !== FALSE){
					$n++;
					
					if(!$header){
						
						$converted_header = $this->convert_csv_names_to_field_names($row);
						if(! in_array('mls_id',$converted_header)){
							echo "<div>Invalid File Type. Make sure you are uploading a tab separated file: ".implode('\t',$row)."</div>";
							return false;
						}
						$header = $converted_header;
					}else{
						
						
						$current_row = @array_combine($header, $row);
						
						
						if(! $current_row){
							$this->faulty_rows[] = $n;
						}else{
							$data[] = $current_row;
						}
						
					}
				}
				fclose($handle);
			}
			return $data;
		}
		
		function is_table_field($tbl, $field){
			//checks if field exists in table.  tbl is table name and field is field name
			global $aspk_db;
			
			if(! isset($this->tbl_fileds[$tbl])){
				$fields = $aspk_db->get_col( "DESC " . $tbl, 0 );
				$this->tbl_fileds[$tbl] = $fields;
			}else{
				$fields = $this->tbl_fileds[$tbl];
			}

			if(in_array($field, $fields)) return true;
			return false;
		}
		
		function is_set_db_settings(){
			$s = get_option( '_aspk_mls_db_settings', array('password' => '' ));
			if(empty($s['password'])) return false;
			return true;
		}
		
		function convert_csv_names_to_field_names($header){
			//header is an array filled with CSV col names
			
			$mapping = $this->get_csv_to_field_mapping();
			
			$new_header = array();
			
			foreach($header as $col){
			
				$col = $this->trim_utf8_bom(strtolower($col));
				
				if (array_key_exists($col, $mapping)) {
					$map_obj = $mapping[$col];
					$new_header[] = $map_obj->field_name;
				}else{
					$new_header[] = $col;
				}
				
			}
			
			return $new_header;
		}
		
		function trim_utf8_bom($text){ 
		   $bom = pack('H*','EFBBBF');
			$text = preg_replace("/^$bom/", '', $text);
			return $text;
		}
		
		
		function get_csv_to_field_mapping(){
			global $aspk_db;
			
			$sql = "Select LOWER(csv_name) as csv_name, field_name, d_type  from mls_field_mapping";
			
			$mapping = $aspk_db->get_results($sql, OBJECT_K);
			
			
			return $mapping;
		}
		
		function get_date_field_mapping(){
			global $aspk_db;
			
			$sql = "Select field_name, LOWER(csv_name) as csv_name, d_type  from mls_field_mapping";
			
			$mapping = $aspk_db->get_results($sql, OBJECT_K);
			$this->field_mapping = $mapping;
			
			return $mapping;
		}
		
		
	} //class ends
} //class exists ends
if(! isset($aspk_db)) $aspk_db = null;

$mls_importer = new Mls_Csv_Importer();

?>
