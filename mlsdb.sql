--
-- Table structure for table `mls_data`
--

CREATE TABLE IF NOT EXISTS `mls_data` (
  `zip_code` varchar(255) DEFAULT NULL,
  `year_built` varchar(255) DEFAULT NULL,
  `withdrawn_dom` int(11) DEFAULT NULL,
  `withdrawn_date` date DEFAULT NULL,
  `waterfront_ft` float DEFAULT NULL,
  `variable_rate_comm` varchar(3) NOT NULL DEFAULT 'no' COMMENT 'yes/no',
  `unit_num` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `total_half_baths` int(11) DEFAULT NULL,
  `total_full_baths` int(11) DEFAULT NULL,
  `total_dom` int(11) DEFAULT NULL,
  `total_bedrooms` int(11) DEFAULT NULL,
  `terms` varchar(255) DEFAULT NULL,
  `taxes` decimal(10,0) DEFAULT NULL,
  `tax_yr` int(11) DEFAULT NULL,
  `tax_id` varchar(255) DEFAULT NULL,
  `subd_complex` varchar(255) DEFAULT NULL,
  `style_1` varchar(255) DEFAULT NULL,
  `style_2` varchar(255) DEFAULT NULL,
  `street_type` varchar(255) DEFAULT NULL,
  `street_num_disp` varchar(255) DEFAULT NULL,
  `street_num` varchar(255) DEFAULT NULL,
  `street_name` varchar(255) DEFAULT NULL,
  `street_dir_suffix` varchar(255) DEFAULT NULL,
  `street_dir_prefix` varchar(255) DEFAULT NULL,
  `status_change_date` date DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `square_footage` varchar(255) DEFAULT NULL,
  `sqft_source` varchar(255) DEFAULT NULL,
  `spcl_1` varchar(255) DEFAULT NULL,
  `spcl_2` varchar(255) DEFAULT NULL,
  `spcl_3` varchar(255) DEFAULT NULL,
  `spcl_4` varchar(255) DEFAULT NULL,
  `show_1` varchar(255) DEFAULT NULL,
  `show_2` varchar(255) DEFAULT NULL,
  `show_3` varchar(255) DEFAULT NULL,
  `show_4` varchar(255) DEFAULT NULL,
  `show_5` varchar(255) DEFAULT NULL,
  `setting_1` varchar(255) DEFAULT NULL,
  `setting_2` varchar(255) DEFAULT NULL,
  `setting_3` varchar(255) DEFAULT NULL,
  `sell_office_phone` varchar(255) DEFAULT NULL,
  `sell_office_name` varchar(255) DEFAULT NULL,
  `sell_broker_code` varchar(255) DEFAULT NULL,
  `sell_agent_phone` varchar(255) DEFAULT NULL,
  `sell_agent_name` varchar(255) DEFAULT NULL,
  `sell_agent_id` varchar(255) DEFAULT NULL,
  `sell_firm_code` varchar(255) DEFAULT NULL,
  `sell_broker_present_offer` varchar(3) NOT NULL DEFAULT 'no' COMMENT 'yes/no',
  `section_gmd` varchar(255) DEFAULT NULL,
  `search_area` varchar(255) DEFAULT NULL,
  `school_bus_route_middle` varchar(3) NOT NULL DEFAULT 'no' COMMENT 'yes/no',
  `school_bus_route_high` varchar(3) NOT NULL DEFAULT 'no' COMMENT 'yes/no',
  `school_bus_route_elem` varchar(3) NOT NULL DEFAULT 'no' COMMENT 'yes/no',
  `sales_price` decimal(10,0) DEFAULT NULL,
  `price_per_sqft` decimal(10,0) DEFAULT NULL,
  `sp_to_lp` decimal(10,0) DEFAULT NULL,
  `sp_to_olp` decimal(10,0) DEFAULT NULL,
  `roof_type_1` varchar(255) DEFAULT NULL,
  `roof_type_2` varchar(255) DEFAULT NULL,
  `roof_type_3` varchar(255) DEFAULT NULL,
  `road` varchar(255) DEFAULT NULL,
  `road_2` varchar(255) DEFAULT NULL,
  `prop_cls_dte` date DEFAULT NULL,
  `prop_type` varchar(255) DEFAULT NULL,
  `price_change_date` date DEFAULT NULL,
  `previous_status` varchar(255) DEFAULT NULL,
  `prevlp` decimal(10,0) DEFAULT NULL,
  `platbook_page` varchar(255) DEFAULT NULL,
  `platbook_book` varchar(255) DEFAULT NULL,
  `photo_count` int(11) DEFAULT NULL,
  `parking_desc_1` varchar(255) DEFAULT NULL,
  `parking_desc_2` varchar(255) DEFAULT NULL,
  `parking_desc_3` varchar(255) DEFAULT NULL,
  `parking_desc_4` varchar(255) DEFAULT NULL,
  `owner_will_take_2nd` varchar(3) NOT NULL DEFAULT 'no' COMMENT 'yes/no',
  `owner_phone` varchar(255) DEFAULT NULL,
  `owner_name` varchar(255) DEFAULT NULL,
  `owner_financing` varchar(3) NOT NULL DEFAULT 'no' COMMENT 'yes/no',
  `orig_list_price` decimal(10,0) DEFAULT NULL,
  `listing_office_website` varchar(255) DEFAULT NULL,
  `offer_date` date DEFAULT NULL,
  `middle_school` varchar(255) DEFAULT NULL,
  `lot_dimensions` varchar(255) DEFAULT NULL,
  `lot_num` varchar(255) DEFAULT NULL,
  `list_office_phone` varchar(255) DEFAULT NULL,
  `list_office_name` varchar(255) DEFAULT NULL,
  `list_office_fax` varchar(255) DEFAULT NULL,
  `list_office_code` varchar(255) DEFAULT NULL,
  `list_agent_e-mail` varchar(255) DEFAULT NULL,
  `list_agent_phone` varchar(255) DEFAULT NULL,
  `list_agent_name` varchar(255) DEFAULT NULL,
  `list_agent_mobile` varchar(255) DEFAULT NULL,
  `list_agent_id` varchar(255) DEFAULT NULL,
  `list_agent_home_phone` varchar(255) DEFAULT NULL,
  `list_agent_fax` varchar(255) DEFAULT NULL,
  `list_src` varchar(255) DEFAULT NULL,
  `list_price` decimal(10,0) DEFAULT NULL,
  `list_firm_code` varchar(255) DEFAULT NULL,
  `list_date` date DEFAULT NULL,
  `lender_mediated` varchar(3) NOT NULL DEFAULT 'no' COMMENT 'yes/no',
  `lastimgtransdate` date DEFAULT NULL,
  `landlot` varchar(255) DEFAULT NULL,
  `lake_name` varchar(255) DEFAULT NULL,
  `list_price_per_sqft` decimal(10,0) DEFAULT NULL,
  `high_school` varchar(255) DEFAULT NULL,
  `heat_1` varchar(255) DEFAULT NULL,
  `heat_2` varchar(255) DEFAULT NULL,
  `heat_3` varchar(255) DEFAULT NULL,
  `heat_4` varchar(255) DEFAULT NULL,
  `num_fireplaces` int(11) DEFAULT NULL,
  `mls_id` bigint(11) NOT NULL,
  `exp_dom` int(11) DEFAULT NULL,
  `expire_date` date DEFAULT NULL,
  `energy` varchar(255) DEFAULT NULL,
  `energy_2` varchar(255) DEFAULT NULL,
  `energy_3` varchar(255) DEFAULT NULL,
  `energy_4` varchar(255) DEFAULT NULL,
  `energy_5` varchar(255) DEFAULT NULL,
  `energy_6` varchar(255) DEFAULT NULL,
  `elementary_school` varchar(255) DEFAULT NULL,
  `duplicate_mls_id` varchar(255) DEFAULT NULL,
  `due_dilig_end` date DEFAULT NULL,
  `doc_folder_pub_count` int(11) DEFAULT NULL,
  `doc_folder_private_count` int(11) DEFAULT NULL,
  `doc_count` int(11) DEFAULT NULL,
  `dock_1` varchar(255) DEFAULT NULL,
  `dock_2` varchar(255) DEFAULT NULL,
  `district` varchar(255) DEFAULT NULL,
  `handicap_1` varchar(255) DEFAULT NULL,
  `handicap_2` varchar(255) DEFAULT NULL,
  `handicap_3` varchar(255) DEFAULT NULL,
  `handicap_4` varchar(255) DEFAULT NULL,
  `handicap_5` varchar(255) DEFAULT NULL,
  `handicap_6` varchar(255) DEFAULT NULL,
  `handicap_7` varchar(255) DEFAULT NULL,
  `handicap_8` varchar(255) DEFAULT NULL,
  `handicap_9` varchar(255) DEFAULT NULL,
  `handicap_10` varchar(255) DEFAULT NULL,
  `deed_page` varchar(255) DEFAULT NULL,
  `deed_book` varchar(255) DEFAULT NULL,
  `days_to_expire` int(11) DEFAULT NULL,
  `days_on_market` int(11) DEFAULT NULL,
  `display_private_remarks` varchar(3) NOT NULL DEFAULT 'no' COMMENT 'yes/no',
  `county` varchar(255) DEFAULT NULL,
  `did_seller_pay_cc` varchar(3) NOT NULL DEFAULT 'no' COMMENT 'yes/no',
  `costs_paid_by_seller` decimal(10,0) DEFAULT NULL,
  `construction_1` varchar(255) NOT NULL,
  `construction_2` varchar(255) DEFAULT NULL,
  `construction_3` varchar(255) DEFAULT NULL,
  `commission_to_selling_broker` decimal(10,0) DEFAULT NULL,
  `co-list_agent_e-mail` varchar(255) DEFAULT NULL,
  `co-list_agent_phone` varchar(255) DEFAULT NULL,
  `co-list_agent_name` varchar(255) DEFAULT NULL,
  `co-list_agent_mobile` varchar(255) DEFAULT NULL,
  `co-list_agent_id` varchar(255) DEFAULT NULL,
  `co-list_agent_home_phone` varchar(255) DEFAULT NULL,
  `co-list_agent_fax` varchar(255) DEFAULT NULL,
  `closing_date` date DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `block` varchar(255) DEFAULT NULL,
  `binding_agreement_date` date DEFAULT NULL,
  `bedrooms_1` varchar(255) DEFAULT NULL,
  `bedrooms_2` varchar(255) DEFAULT NULL,
  `bedrooms_3` varchar(255) DEFAULT NULL,
  `basement_desc_1` varchar(255) DEFAULT NULL,
  `basement_desc_2` varchar(255) DEFAULT NULL,
  `basement_desc_3` varchar(255) DEFAULT NULL,
  `basement_desc_4` varchar(255) DEFAULT NULL,
  `basement_desc_5` varchar(255) DEFAULT NULL,
  `basement_desc_6` varchar(255) DEFAULT NULL,
  `avail_to_rec_offers` varchar(3) NOT NULL DEFAULT 'no' COMMENT 'yes/no',
  `assume` varchar(3) NOT NULL DEFAULT 'no' COMMENT 'yes/no',
  `assoc_fee_desc` varchar(255) DEFAULT NULL,
  `area_01` varchar(255) DEFAULT NULL,
  `area_02` varchar(255) DEFAULT NULL,
  `area_03` varchar(255) DEFAULT NULL,
  `area_04` varchar(255) DEFAULT NULL,
  `area_05` varchar(255) DEFAULT NULL,
  `area_06` varchar(255) DEFAULT NULL,
  `area_07` varchar(255) DEFAULT NULL,
  `area_08` varchar(255) DEFAULT NULL,
  `area_09` varchar(255) DEFAULT NULL,
  `area_10` varchar(255) DEFAULT NULL,
  `lot_size` varchar(255) DEFAULT NULL,
  `association_fee` decimal(10,0) DEFAULT NULL,
  `age_desc` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `acres` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`mls_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mls_data`
--


-- --------------------------------------------------------

--
-- Table structure for table `mls_data_fields`
--

CREATE TABLE IF NOT EXISTS `mls_data_fields` (
  `field_name` varchar(255) DEFAULT NULL,
  `data_type` varchar(255) DEFAULT NULL,
  `display_label` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mls_data_fields`
--


-- --------------------------------------------------------

--
-- Table structure for table `mls_field_mapping`
--

CREATE TABLE IF NOT EXISTS `mls_field_mapping` (
  `csv_name` varchar(255) NOT NULL,
  `field_name` varchar(255) NOT NULL,
  `d_type` varchar(255) NOT NULL,
  `display_label` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  PRIMARY KEY (`field_name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mls_field_mapping`
--

INSERT INTO `mls_field_mapping` VALUES('Acres', 'acres', 'decimal', 'Acres', '');
INSERT INTO `mls_field_mapping` VALUES('Address', 'address', 'varchar', 'Address', '');
INSERT INTO `mls_field_mapping` VALUES('Age Desc', 'age_desc', 'varchar', 'Age Description', '');
INSERT INTO `mls_field_mapping` VALUES('Area', 'area_01', 'varchar', 'Area 1', '');
INSERT INTO `mls_field_mapping` VALUES('Area_2', 'area_02', 'varchar', 'Area 2', '');
INSERT INTO `mls_field_mapping` VALUES('Area_3', 'area_03', 'varchar', 'Area 3', '');
INSERT INTO `mls_field_mapping` VALUES('Area_4', 'area_04', 'varchar', 'Area 4', '');
INSERT INTO `mls_field_mapping` VALUES('Area_5', 'area_05', 'varchar', 'Area 5', '');
INSERT INTO `mls_field_mapping` VALUES('Area_6', 'area_06', 'varchar', 'Area 6', '');
INSERT INTO `mls_field_mapping` VALUES('Area_7', 'area_07', 'varchar', 'Area 7', '');
INSERT INTO `mls_field_mapping` VALUES('Area_8', 'area_08', 'varchar', 'Area 8', '');
INSERT INTO `mls_field_mapping` VALUES('Area_9', 'area_09', 'varchar', 'Area 9', '');
INSERT INTO `mls_field_mapping` VALUES('Area_10', 'area_10', 'varchar', 'Area 10', '');
INSERT INTO `mls_field_mapping` VALUES('Assc Fee', 'association_fee', 'decimal', 'Association Fee', '');
INSERT INTO `mls_field_mapping` VALUES('Assoc Fee Desc', 'assoc_fee_desc', 'varchar', 'Association Fee Description', '');
INSERT INTO `mls_field_mapping` VALUES('Assume', 'assume', 'boolean', 'Mortgage is Assumable', '');
INSERT INTO `mls_field_mapping` VALUES('Avail to Rec Offers', 'avail_to_rec_offers', 'boolean', 'Available to Receive Offers', '');
INSERT INTO `mls_field_mapping` VALUES('Basement Desc', 'basement_desc_1', 'varchar', 'Basement Description 1', '');
INSERT INTO `mls_field_mapping` VALUES('Basement Desc_2', 'basement_desc_2', 'varchar', 'Basement Description 2', '');
INSERT INTO `mls_field_mapping` VALUES('Basement Desc_3', 'basement_desc_3', 'varchar', 'Basement Description 3', '');
INSERT INTO `mls_field_mapping` VALUES('Basement Desc_4', 'basement_desc_4', 'varchar', 'Basement Description 4', '');
INSERT INTO `mls_field_mapping` VALUES('Basement Desc_5', 'basement_desc_5', 'varchar', 'Basement Description 5', '');
INSERT INTO `mls_field_mapping` VALUES('Basement Desc_6', 'basement_desc_6', 'varchar', 'Basement Description 6', '');
INSERT INTO `mls_field_mapping` VALUES('Bdrm', 'bedrooms_1', 'varchar', 'Bedroom Options 1', '');
INSERT INTO `mls_field_mapping` VALUES('Bdrm_2', 'bedrooms_2', 'varchar', 'Bedroom Options 2', '');
INSERT INTO `mls_field_mapping` VALUES('Bdrm_3', 'bedrooms_3', 'varchar', 'Bedroom Options 3', '');
INSERT INTO `mls_field_mapping` VALUES('Binding Agreement Date', 'binding_agreement_date', 'date', 'Binding Agreement Date', '');
INSERT INTO `mls_field_mapping` VALUES('Block', 'block', 'varchar', 'Block', '');
INSERT INTO `mls_field_mapping` VALUES('City', 'city', 'varchar', 'City', '');
INSERT INTO `mls_field_mapping` VALUES('Closing Date', 'closing_date', 'date', 'Closing Date', '');
INSERT INTO `mls_field_mapping` VALUES('Co-List Agent e-mail', 'co-list_agent_e-mail', 'varchar', 'Co-Listing Agent Email', '');
INSERT INTO `mls_field_mapping` VALUES('Co-List Agent Fax', 'co-list_agent_fax', 'varchar', 'Co-Listing Agent Fax', '');
INSERT INTO `mls_field_mapping` VALUES('Co-List Agent Home Phone', 'co-list_agent_home_phone', 'varchar', 'Co-Listing Agent Home Phone', '');
INSERT INTO `mls_field_mapping` VALUES('Co-List Agent ID', 'co-list_agent_id', 'varchar', 'Co-Listing Agent ID', '');
INSERT INTO `mls_field_mapping` VALUES('Co-List Agent Mobile', 'co-list_agent_mobile', 'varchar', 'Co-Listing Agent Mobile Phone', '');
INSERT INTO `mls_field_mapping` VALUES('Co-List Agent Name', 'co-list_agent_name', 'varchar', 'Co-Listing Agent Name', '');
INSERT INTO `mls_field_mapping` VALUES('Co-List Agent Phone', 'co-list_agent_phone', 'varchar', 'Co-Listing Agent Phone', '');
INSERT INTO `mls_field_mapping` VALUES('Commission to Selling Broker', 'commission_to_selling_broker', 'decimal', 'Selling Broker Commission', '');
INSERT INTO `mls_field_mapping` VALUES('Cnstr', 'construction_1', 'varchar', 'Construction 1', '');
INSERT INTO `mls_field_mapping` VALUES('Cnstr_2', 'construction_2', 'varchar', 'Construction 2', '');
INSERT INTO `mls_field_mapping` VALUES('Cnstr_3', 'construction_3', 'varchar', 'Construction 3', '');
INSERT INTO `mls_field_mapping` VALUES('Costs Paid by Seller', 'costs_paid_by_seller', 'decimal', 'Seller Paid Closing Costs', '');
INSERT INTO `mls_field_mapping` VALUES('County', 'county', 'varchar', 'County', '');
INSERT INTO `mls_field_mapping` VALUES('DOM', 'days_on_market', 'int', 'Days on Market', '');
INSERT INTO `mls_field_mapping` VALUES('Days to Expire', 'days_to_expire', 'int', 'Days to Expire', '');
INSERT INTO `mls_field_mapping` VALUES('DeedBk', 'deed_book', 'varchar', 'Deed Book', '');
INSERT INTO `mls_field_mapping` VALUES('DeedPg', 'deed_page', 'varchar', 'Deed Page', '');
INSERT INTO `mls_field_mapping` VALUES('CC', 'did_seller_pay_cc', 'boolean', 'Did Seller Pay Closing Costs', '');
INSERT INTO `mls_field_mapping` VALUES('DPR Y/N', 'display_private_remarks', 'boolean', 'Display Private Remarks (???)', '');
INSERT INTO `mls_field_mapping` VALUES('Dist', 'district', 'varchar', 'District', '');
INSERT INTO `mls_field_mapping` VALUES('Dock', 'dock_1', 'varchar', 'Dock 1', '');
INSERT INTO `mls_field_mapping` VALUES('Dock_2', 'dock_2', 'varchar', 'Dock 2', '');
INSERT INTO `mls_field_mapping` VALUES('Doc Count', 'doc_count', 'int', 'Documents', '');
INSERT INTO `mls_field_mapping` VALUES('Doc Folder Private Count', 'doc_folder_private_count', 'int', 'Private Documents', '');
INSERT INTO `mls_field_mapping` VALUES('Doc Folder Pub Count', 'doc_folder_pub_count', 'int', 'Public Documents', '');
INSERT INTO `mls_field_mapping` VALUES('Due Dilig End', 'due_dilig_end', 'date', 'Due Diligence End Date', '');
INSERT INTO `mls_field_mapping` VALUES('Duplicate FMLS #', 'duplicate_mls_id', 'varchar', 'Duplicate MLS ID#', '');
INSERT INTO `mls_field_mapping` VALUES('ELEM', 'elementary_school', 'varchar', 'Elementary School', '');
INSERT INTO `mls_field_mapping` VALUES('Energy', 'energy', 'varchar', 'Energy', '');
INSERT INTO `mls_field_mapping` VALUES('Energy_2', 'energy_2', 'varchar', 'Energy 2', '');
INSERT INTO `mls_field_mapping` VALUES('Energy_3', 'energy_3', 'varchar', 'Energy 3', '');
INSERT INTO `mls_field_mapping` VALUES('Energy_4', 'energy_4', 'varchar', 'Energy 4', '');
INSERT INTO `mls_field_mapping` VALUES('Energy_5', 'energy_5', 'varchar', 'Energy 5', '');
INSERT INTO `mls_field_mapping` VALUES('Energy_6', 'energy_6', 'varchar', 'Energy 6', '');
INSERT INTO `mls_field_mapping` VALUES('Expire Date', 'expire_date', 'date', 'Expiration Date', '');
INSERT INTO `mls_field_mapping` VALUES('Exp DOM', 'exp_dom', 'int', 'Expired Days on Market', '');
INSERT INTO `mls_field_mapping` VALUES('Hndcap', 'handicap_1', 'varchar', 'Handicap 1', '');
INSERT INTO `mls_field_mapping` VALUES('Hndcap_10', 'handicap_10', 'varchar', 'Handicap 10', '');
INSERT INTO `mls_field_mapping` VALUES('Hndcap_2', 'handicap_2', 'varchar', 'Handicap 2', '');
INSERT INTO `mls_field_mapping` VALUES('Hndcap_3', 'handicap_3', 'varchar', 'Handicap 3', '');
INSERT INTO `mls_field_mapping` VALUES('Hndcap_4', 'handicap_4', 'varchar', 'Handicap 4', '');
INSERT INTO `mls_field_mapping` VALUES('Hndcap_5', 'handicap_5', 'varchar', 'Handicap 5', '');
INSERT INTO `mls_field_mapping` VALUES('Hndcap_6', 'handicap_6', 'varchar', 'Handicap 6', '');
INSERT INTO `mls_field_mapping` VALUES('Hndcap_7', 'handicap_7', 'varchar', 'Handicap 7', '');
INSERT INTO `mls_field_mapping` VALUES('Hndcap_8', 'handicap_8', 'varchar', 'Handicap 8', '');
INSERT INTO `mls_field_mapping` VALUES('Hndcap_9', 'handicap_9', 'varchar', 'Handicap 9', '');
INSERT INTO `mls_field_mapping` VALUES('Heat', 'heat_1', 'varchar', 'Heating Options 1', '');
INSERT INTO `mls_field_mapping` VALUES('Heat_2', 'heat_2', 'varchar', 'Heating Options 2', '');
INSERT INTO `mls_field_mapping` VALUES('Heat_3', 'heat_3', 'varchar', 'Heating Options 3', '');
INSERT INTO `mls_field_mapping` VALUES('Heat_4', 'heat_4', 'varchar', 'Heating Options 4', '');
INSERT INTO `mls_field_mapping` VALUES('High', 'high_school', 'varchar', 'High School', '');
INSERT INTO `mls_field_mapping` VALUES('Lake Nm', 'lake_name', 'varchar', 'Lake Name', '');
INSERT INTO `mls_field_mapping` VALUES('Landlot', 'landlot', 'varchar', 'Land Lot', '');
INSERT INTO `mls_field_mapping` VALUES('LastImgTransDate', 'lastimgtransdate', 'date', 'Last Image Transaction Date', '');
INSERT INTO `mls_field_mapping` VALUES('Lender Mediated', 'lender_mediated', 'boolean', 'Lender Mediated', '');
INSERT INTO `mls_field_mapping` VALUES('Office List Web', 'listing_office_website', 'varchar', 'Listing Office Website', '');
INSERT INTO `mls_field_mapping` VALUES('List Agent e-mail', 'list_agent_e-mail', 'varchar', 'Listing Agent Email', '');
INSERT INTO `mls_field_mapping` VALUES('Agt Fax', 'list_agent_fax', 'varchar', 'Listing Agent Fax', '');
INSERT INTO `mls_field_mapping` VALUES('List Agent Home Phone', 'list_agent_home_phone', 'varchar', 'Listing Agent Home Phone', '');
INSERT INTO `mls_field_mapping` VALUES('List Agent ID', 'list_agent_id', 'varchar', 'Listing Agent ID', '');
INSERT INTO `mls_field_mapping` VALUES('List Agent Mobile', 'list_agent_mobile', 'varchar', 'Listing Agent Mobile Phone', '');
INSERT INTO `mls_field_mapping` VALUES('List Agent Name', 'list_agent_name', 'varchar', 'Listing Agent Name', '');
INSERT INTO `mls_field_mapping` VALUES('List Agent Phone', 'list_agent_phone', 'varchar', 'Listing Agent Phone', '');
INSERT INTO `mls_field_mapping` VALUES('Lst Dte', 'list_date', 'date', 'List Date', '');
INSERT INTO `mls_field_mapping` VALUES('List Firm Code', 'list_firm_code', 'varchar', 'Listing Firm Code', '');
INSERT INTO `mls_field_mapping` VALUES('Listing Office Code', 'list_office_code', 'varchar', 'Listing Office Code', '');
INSERT INTO `mls_field_mapping` VALUES('List Office Fax', 'list_office_fax', 'varchar', 'Listing Office Fax', '');
INSERT INTO `mls_field_mapping` VALUES('List Office Name', 'list_office_name', 'varchar', 'Listing Office Name', '');
INSERT INTO `mls_field_mapping` VALUES('List Office Phone', 'list_office_phone', 'varchar', 'Listing Office Phone', '');
INSERT INTO `mls_field_mapping` VALUES('LP', 'list_price', 'decimal', 'List Price', '');
INSERT INTO `mls_field_mapping` VALUES('LP/Sqft', 'list_price_per_sqft', 'decimal', 'List Price per Square Foot', '');
INSERT INTO `mls_field_mapping` VALUES('LstSrc', 'list_src', 'varchar', 'Listing Source', '');
INSERT INTO `mls_field_mapping` VALUES('Lot Dim', 'lot_dimensions', 'varchar', 'Lot Dimensions', '');
INSERT INTO `mls_field_mapping` VALUES('Lot', 'lot_num', 'varchar', 'Lot Number', '');
INSERT INTO `mls_field_mapping` VALUES('Lot Sz', 'lot_size', 'varchar', 'Lot Size', '');
INSERT INTO `mls_field_mapping` VALUES('Middle', 'middle_school', 'varchar', 'Middle School', '');
INSERT INTO `mls_field_mapping` VALUES('FMLS#', 'mls_id', 'primary_key', 'MLS ID#', '');
INSERT INTO `mls_field_mapping` VALUES('# FP', 'num_fireplaces', 'int', 'Number of Fireplaces', '');
INSERT INTO `mls_field_mapping` VALUES('Offer Date', 'offer_date', 'date', 'Offer Date', '');
INSERT INTO `mls_field_mapping` VALUES('Orig LP', 'orig_list_price', 'decimal', 'Original List Price', '');
INSERT INTO `mls_field_mapping` VALUES('Owner Financing?', 'owner_financing', 'boolean', 'Owner Financing', '');
INSERT INTO `mls_field_mapping` VALUES('Own Nm', 'owner_name', 'varchar', 'Owner Name', '');
INSERT INTO `mls_field_mapping` VALUES('Own Phn', 'owner_phone', 'varchar', 'Owner Phone', '');
INSERT INTO `mls_field_mapping` VALUES('Owner 2nd', 'owner_will_take_2nd', 'boolean', 'Owner Will Finance with 2nd Mortgage', '');
INSERT INTO `mls_field_mapping` VALUES('Parking Desc', 'parking_desc_1', 'varchar', 'Parking Description', '');
INSERT INTO `mls_field_mapping` VALUES('Parking Desc_2', 'parking_desc_2', 'varchar', 'Parking Description 2', '');
INSERT INTO `mls_field_mapping` VALUES('Parking Desc_3', 'parking_desc_3', 'varchar', 'Parking Description 3', '');
INSERT INTO `mls_field_mapping` VALUES('Parking Desc_4', 'parking_desc_4', 'varchar', 'Parking Description 4', '');
INSERT INTO `mls_field_mapping` VALUES('Photo Count', 'photo_count', 'int', 'Photo Count', '');
INSERT INTO `mls_field_mapping` VALUES('Book', 'platbook_book', 'varchar', 'Platbook Book #', '');
INSERT INTO `mls_field_mapping` VALUES('Page', 'platbook_page', 'varchar', 'Platbook Page #', '');
INSERT INTO `mls_field_mapping` VALUES('Previous Status', 'previous_status', 'varchar', 'Previous Status', '');
INSERT INTO `mls_field_mapping` VALUES('PrevLP', 'prevlp', 'decimal', 'Previous List Price', '');
INSERT INTO `mls_field_mapping` VALUES('Price Change Date', 'price_change_date', 'date', 'Price Change Date', '');
INSERT INTO `mls_field_mapping` VALUES('SP/Sqft', 'price_per_sqft', 'decimal', 'Sales Price per Square Foot', '');
INSERT INTO `mls_field_mapping` VALUES('Prop Cls Dte', 'prop_cls_dte', 'date', 'Proposed Close Date', '');
INSERT INTO `mls_field_mapping` VALUES('Prop Type', 'prop_type', 'varchar', 'Property Type', '');
INSERT INTO `mls_field_mapping` VALUES('Road', 'road', 'varchar', 'Road', '');
INSERT INTO `mls_field_mapping` VALUES('Road_2', 'road_2', 'varchar', 'Road 2', '');
INSERT INTO `mls_field_mapping` VALUES('Roof Type', 'roof_type_1', 'varchar', 'Roof Type', '');
INSERT INTO `mls_field_mapping` VALUES('Roof Type_2', 'roof_type_2', 'varchar', 'Roof Type 2', '');
INSERT INTO `mls_field_mapping` VALUES('Roof Type_3', 'roof_type_3', 'varchar', 'Roof Type 3', '');
INSERT INTO `mls_field_mapping` VALUES('Sales Price', 'sales_price', 'decimal', 'Sales Price', '');
INSERT INTO `mls_field_mapping` VALUES('School Bus Route Elem', 'school_bus_route_elem', 'boolean', 'On Elementary School Bus Route', '');
INSERT INTO `mls_field_mapping` VALUES('School Bus Route High', 'school_bus_route_high', 'boolean', 'On High School Bus Route', '');
INSERT INTO `mls_field_mapping` VALUES('School Bus Route Middle', 'school_bus_route_middle', 'boolean', 'On Middle School Bus Route', '');
INSERT INTO `mls_field_mapping` VALUES('Search Area', 'search_area', 'varchar', 'Search Area', '');
INSERT INTO `mls_field_mapping` VALUES('Sect/GMD', 'section_gmd', 'varchar', 'Section / GMD', '');
INSERT INTO `mls_field_mapping` VALUES('Selling Agent ID', 'sell_agent_id', 'varchar', 'Selling Agent ID', '');
INSERT INTO `mls_field_mapping` VALUES('Sell Agent Name', 'sell_agent_name', 'varchar', 'Selling Agent Name', '');
INSERT INTO `mls_field_mapping` VALUES('Sell Agent Phone', 'sell_agent_phone', 'varchar', 'Selling Agent Phone', '');
INSERT INTO `mls_field_mapping` VALUES('SellingBrokerCode', 'sell_broker_code', 'varchar', 'Selling Broker Code', '');
INSERT INTO `mls_field_mapping` VALUES('Sell Brkr/Agent present offer', 'sell_broker_present_offer', 'boolean', 'Can Selling Broker Present Offers', '');
INSERT INTO `mls_field_mapping` VALUES('Sell Firm Code', 'sell_firm_code', 'varchar', 'Selling Firm Code', '');
INSERT INTO `mls_field_mapping` VALUES('Sell Office Name', 'sell_office_name', 'varchar', 'Selling Office Name', '');
INSERT INTO `mls_field_mapping` VALUES('Sell Office Phone', 'sell_office_phone', 'varchar', 'Selling Office Phone', '');
INSERT INTO `mls_field_mapping` VALUES('Setting', 'setting_1', 'varchar', 'Setting 1', '');
INSERT INTO `mls_field_mapping` VALUES('Setting_2', 'setting_2', 'varchar', 'Setting 2', '');
INSERT INTO `mls_field_mapping` VALUES('Setting_3', 'setting_3', 'varchar', 'Setting 3', '');
INSERT INTO `mls_field_mapping` VALUES('Show', 'show_1', 'varchar', 'Show 1', '');
INSERT INTO `mls_field_mapping` VALUES('Show_2', 'show_2', 'varchar', 'Show 2', '');
INSERT INTO `mls_field_mapping` VALUES('Show_3', 'show_3', 'varchar', 'Show 3', '');
INSERT INTO `mls_field_mapping` VALUES('Show_4', 'show_4', 'varchar', 'Show 4', '');
INSERT INTO `mls_field_mapping` VALUES('Show_5', 'show_5', 'varchar', 'Show 5', '');
INSERT INTO `mls_field_mapping` VALUES('Spcl', 'spcl_1', 'varchar', 'Special 1', '');
INSERT INTO `mls_field_mapping` VALUES('Spcl_2', 'spcl_2', 'varchar', 'Special 2', '');
INSERT INTO `mls_field_mapping` VALUES('Spcl_3', 'spcl_3', 'varchar', 'Special 3', '');
INSERT INTO `mls_field_mapping` VALUES('Spcl_4', 'spcl_4', 'varchar', 'Special 4', '');
INSERT INTO `mls_field_mapping` VALUES('SP$/LP$', 'sp_to_lp', 'decimal', 'Sales Price to List Price', '');
INSERT INTO `mls_field_mapping` VALUES('SP$/$OLP', 'sp_to_olp', 'decimal', 'Sales Price to Original List Price', '');
INSERT INTO `mls_field_mapping` VALUES('SqFt Source', 'sqft_source', 'varchar', 'SqFt Source', '');
INSERT INTO `mls_field_mapping` VALUES('Square Footage', 'square_footage', 'decimal', 'Square Footage (SqFt)', '');
INSERT INTO `mls_field_mapping` VALUES('State', 'state', 'varchar', 'State', '');
INSERT INTO `mls_field_mapping` VALUES('Status', 'status', 'varchar', 'Status', '');
INSERT INTO `mls_field_mapping` VALUES('Status Change Date', 'status_change_date', 'date', 'Status Change Date', '');
INSERT INTO `mls_field_mapping` VALUES('Street Dir Prefix', 'street_dir_prefix', 'varchar', 'Street Direction Prefix', '');
INSERT INTO `mls_field_mapping` VALUES('Street Dir Suffix', 'street_dir_suffix', 'varchar', 'Street Direction Suffix', '');
INSERT INTO `mls_field_mapping` VALUES('St Name', 'street_name', 'varchar', 'Street Name', '');
INSERT INTO `mls_field_mapping` VALUES('Str #', 'street_num', 'varchar', 'Street #', '');
INSERT INTO `mls_field_mapping` VALUES('Str # Disp', 'street_num_disp', 'varchar', 'Street # (Display)', '');
INSERT INTO `mls_field_mapping` VALUES('Street Type', 'street_type', 'varchar', 'Street Type', '');
INSERT INTO `mls_field_mapping` VALUES('Style', 'style_1', 'varchar', 'Style', '');
INSERT INTO `mls_field_mapping` VALUES('Style_2', 'style_2', 'varchar', 'Style 2', '');
INSERT INTO `mls_field_mapping` VALUES('Subd/Complex', 'subd_complex', 'varchar', 'Subdivion/Complex', '');
INSERT INTO `mls_field_mapping` VALUES('CSV Column Name', 'Suggested Field Name', 'Suggested Data Type', 'Display Label', 'Description');
INSERT INTO `mls_field_mapping` VALUES('Taxes', 'taxes', 'decimal', 'Taxes', '');
INSERT INTO `mls_field_mapping` VALUES('Tax ID', 'tax_id', 'varchar', 'Tax ID', '');
INSERT INTO `mls_field_mapping` VALUES('Tax Yr', 'tax_yr', 'int', 'Tax Year', '');
INSERT INTO `mls_field_mapping` VALUES('Terms', 'terms', 'varchar', 'Loan Terms', '');
INSERT INTO `mls_field_mapping` VALUES('Tot Beds', 'total_bedrooms', 'int', 'Total Beds', '');
INSERT INTO `mls_field_mapping` VALUES('Total DOM', 'total_dom', 'int', 'Total Days on Market', '');
INSERT INTO `mls_field_mapping` VALUES('Tot FB', 'total_full_baths', 'int', 'Total Full Baths', '');
INSERT INTO `mls_field_mapping` VALUES('Tot HB', 'total_half_baths', 'int', 'Total Half Baths', '');
INSERT INTO `mls_field_mapping` VALUES('Type', 'type', 'varchar', 'Type', '');
INSERT INTO `mls_field_mapping` VALUES('Unit #', 'unit_num', 'varchar', 'Unit #', '');
INSERT INTO `mls_field_mapping` VALUES('VRC', 'variable_rate_comm', 'boolean', 'Variable Rate Commission', '');
INSERT INTO `mls_field_mapping` VALUES('Wtrft Ft', 'waterfront_ft', 'float', 'Waterfront Footage', '');
INSERT INTO `mls_field_mapping` VALUES('Wth Dte', 'withdrawn_date', 'date', 'Withdrawn Date', '');
INSERT INTO `mls_field_mapping` VALUES('Withdrawn DOM', 'withdrawn_dom', 'int', 'Withdrawn Days on Market', '');
INSERT INTO `mls_field_mapping` VALUES('Yr Blt', 'year_built', 'varchar', 'Year Built', '');
INSERT INTO `mls_field_mapping` VALUES('Zip', 'zip_code', 'varchar', 'Zip Code', '');
